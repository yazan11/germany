import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Home from '../screens/index';
import CountriesList from '../screens/CountriesList';
import CountriesDetaiels from '../screens/CountrieDetaiels';
import Search from '../screens/Search';
import Chart from '../screens/Chart';

const Stack = createStackNavigator();

export default function MyStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomePage">
        <Stack.Screen
          name="HomePage"
          component={Home}
          options={{
            header: ({ }) => null,
          }}
        />
        <Stack.Screen name="CountriesList" component={CountriesList} />
        <Stack.Screen name="CountriesDetaiels" component={CountriesDetaiels} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="Chart" component={Chart} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
